<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 4/30/17
 * Time: 10:21 AM
 */

namespace RCSI\Wrapper;


use RCSI\Config\Config;
use RCSI\Connection\Database\Database;
use RCSI\Connection\Database\DatabaseInterface;

class DatabaseFactory extends Database
{
    /**
     * @var DatabaseInterface
     */
    public static $database;

    /**
     * @param Config $config
     * @return DatabaseInterface
     */
    public static function init(Config $config = null)
    {
        if (self::$database === null) {
            if ($config === null) {
                $config = ConfigWrapper::init();
            }
            Database::setConfig($config);
            self::$database = Database::init();
        }
        return self::$database;
    }

    /**
     * @param Config $config
     * @return DatabaseInterface
     */
    public static function initConfig(Config $config)
    {
        if (self::$database === null) {
            Database::setConfig($config);
            $database = new Database();

            self::$database = $database;
        }
        return self::$database;
    }

    public static function destroy()
    {
        self::$database = null;
    }

}
