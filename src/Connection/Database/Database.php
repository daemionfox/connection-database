<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 2/28/15
 * Time: 2:59 PM
 */

namespace RCSI\Connection\Database;

use PDO;
use RCSI\Config\Config;
use RCSI\Exceptions\ConfigException;
use RCSI\Exceptions\DatabaseException;

/**
 * Class Database
 *
 * @package RCSI\Connection\Database
 */
class Database implements DatabaseInterface
{
    /**
     * @var string
     */
    private $host = 'localhost';
    /**
     * @var string
     */
    private $user = '';
    /**
     * @var string
     */
    private $pass = '';
    /**
     * @var string
     */
    private $database = '';
    /**
     * @var string
     */
    private $prefix = '';
    /**@var string */
    private $dbType = 'mysql';

    /**
     * @var string
     */
    private $dbPath;
    /** @var */
    private $dsn;

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var int
     */
    private $numberExecutions = 0;

    /**
     * @var Database
     */
    private static $instance = null;

    /**
     * @var Config
     */
    private static $config;


    /**
     * Database constructor.
     * @throws DatabaseException
     */
    public function __construct()
    {
        $this->setupConfig();
        $this->createPDO();
    }

    /**
     * @param mixed $dsn
     * @return self
     */
    public function setDsn($dsn)
    {
        $this->dsn = $dsn;
        return $this;
    }

    /**
     * @param string $user
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param string $pass
     * @return self
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
        return $this;
    }

    /**
     * @param $prefix
     * @return $this
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @param Config $config
     */
    public static function setConfig(Config $config)
    {
        self::$config = $config;
    }



    /**
     *
     */
    public function createPDO()
    {
        $this->pdo = new \PDO($this->dsn, $this->user, $this->pass);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @throws DatabaseException
     */
    private function setupConfig(){
        if (is_null(self::$config)) {
            self::$config = Config::init();
        }
        try {
            $this->host     = self::$config->get("databaseHost");
            $this->user     = self::$config->get("databaseUser");
            $this->pass     = self::$config->get("databasePass");
            $this->database = self::$config->get("databaseDBase");
            $this->prefix   = self::$config->get("databasePrefix");
            $this->dbType   = self::$config->get("databaseType");
            $this->dbPath   = self::$config->get("databasePath");

            switch($this->dbType) {
                case "sqlite":
                    if($this->dbPath === null) {
                        throw new DatabaseException("Cannot connect to sqlite database without a path");
                    }
                    $this->dsn = "sqlite:{$this->dbPath}";
                    break;
                case "mysql":
                default:
                    $this->dsn = "{$this->dbType}:dbname={$this->database};host={$this->host}";
                    break;
            }

        }  catch (ConfigException $e) {
            throw new DatabaseException("Database Exception: {$e->getMessage()}");
        }
    }

    /**
     * @param string $type
     * @return Database
     */
    public static function init(){
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param $sql
     * @param $array
     * @return null|\PDOStatement
     * @throws \PDOException
     */
    public function query($sql, $array)
    {
        $statement = null;
        try {
            $statement = $this->pdo->prepare($sql);
            foreach ($array as $key => $value) {
                $statement->bindValue($key, $value, \PDO::PARAM_STR);
            }
            $statement->execute();
            $this->numberExecutions++;
        } catch (\PDOException $e) {
            throw $e;
        }
        return $statement;
    }

    /**
     * @param $sql
     * @param $array
     * @return mixed
     * @throws \PDOException
     */
    public function queryFirst($sql, $array)
    {
        $result = $this->queryAll($sql, $array);
        return array_shift($result);
    }

    /**
     * For Backwards compatibility
     *
     * @param $sql
     * @param $array
     * @return array
     * @throws \PDOException
     */
    public function queryResult($sql, $array)
    {
        return $this->queryAll($sql, $array);
    }

    /**
     * @param $sql
     * @param $array
     * @return array
     * @throws \PDOException
     */
    public function queryAll($sql, $array)
    {
        $result = $this->query($sql, $array);
        return $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $sql
     * @param $array
     * @return mixed
     * @throws \PDOException
     */
    public function queryLast($sql, $array)
    {
        $result = $this->queryAll($sql, $array);
        return array_pop($result);
    }

    /**
     * @return string
     */
    public function prefix()
    {
        return $this->prefix;
    }

    /**
     * @return string
     */
    public function getLastInsertID()
    {
        return $this->pdo->lastInsertID();
    }
} 
