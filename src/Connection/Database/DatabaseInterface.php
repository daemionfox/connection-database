<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 2/13/15
 * Time: 9:47 PM
 */

namespace RCSI\Connection\Database;

interface DatabaseInterface {

    public function query($sql, $array);
    public function queryFirst($sql, $array);
    public function queryResult($sql, $array);
    public function queryAll($sql, $array);
    public function queryLast($sql, $array);
    public function prefix();
    public function getLastInsertID();
} 
